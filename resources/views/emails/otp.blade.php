<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OTP for Password Reset</title>
</head>
<body>
    <p>Hello,</p>
    
    <p>Your OTP for password reset is: <strong>{{ $otp }}</strong></p>

    <p>Please use this OTP to complete the password reset process.</p>

    <p>If you didn't request a password reset, you can safely ignore this email.</p>

    <p>Thank you!</p>
</body>
</html>