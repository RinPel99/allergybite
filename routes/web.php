<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ForgotPasswordController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\ResetPasswordController;
use App\Http\Controllers\RegistrationController;
use Inertia\Inertia;

Route::get('/', [AuthController::class, 'create']);
Route::get('/login', [AuthController::class, 'login'])->name('login');
Route::post('/login', [AuthController::class, 'store']);
Route::delete('/logout', [AuthController::class, 'destroy']);
Route::get('/home', [IndexController::class, 'getAdminHome']);

Route::get('/detail', [IndexController::class, 'getDetail']);

// Forgot Password
Route::get('/forgot-password', [ForgotPasswordController::class, 'showEmailForm'])->name('password.request');
Route::post('/forgot-password', [ForgotPasswordController::class, 'sendResetLinkEmail'])->name('password.email');

// Reset Password
Route::get('/reset-password/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset');
Route::post('/reset-password', [ResetPasswordController::class, 'reset'])->name('password.update');

Route::get('/register', [RegistrationController::class, 'showRegistrationForm'])->name('registration.form');
Route::post('/register', [RegistrationController::class, 'register'])->name('registration.register');
Route::get('/verify', [RegistrationController::class, 'showVerificationForm'])->name('registration.verify');
Route::post('/verify', [RegistrationController::class, 'verify']);
Route::get('/password', [RegistrationController::class, 'showPasswordForm'])->name('registration.password');
Route::post('/password', [RegistrationController::class, 'setPassword']);
Route::get('/allergies', [RegistrationController::class, 'showAllergiesForm'])->name('registration.showAllergiesForm');
Route::post('/allergies', [RegistrationController::class, 'setAllergies']);

//admin route goes here
