<?php

namespace App\Http\Controllers;

use App\Models\User;
use Barryvdh\LaravelIdeHelper\UsesResolver;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function AddUsers(Request $request)
    {
        $data = $request->validate([
            
                'name' => 'required',
                'email' => 'required|email',
                'password' => 'password'
        ]);
        $data['role'] = 'admin';

        $user = User::factory()->create($data);

        return response()->json(['message' => 'User added successfully', 'user' => $user], 201);
    }

    public function deleteUsers($id)
        {
            $item = User::find($id);

            if (!$item) {
                return response()->json(['message' => 'User not found'], 404);
            }

            $item->delete();

            return response()->json(['message' => 'User deleted'], 200);
        }

        public function editUser(Request $request, User $user)
        {
            error_log("test");
            // Validate the request data
            $this->validate($request, [
                'name' => 'required|string',
            ]);

            // Update the user's information
            $user->update([
                'name' => $request->input('name'),
            ]);

            return response()->json(['message' => 'User updated successfully']);
        }   
}
