<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use App\Models\User;
use Inertia\Inertia;
use OtpMail;

class ForgotPasswordController extends Controller
{
    public function showEmailForm()
    {
        return inertia('Auth/forgotPassword');
    }

    public function sendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::where('email', $request->email)->first();

        // Generate and save OTP
        $otp = Str::random(6);
        $user->update(['otp' => $otp]);

        // Send OTP via email
        Mail::to($user->email)->send(new OtpMail($otp));

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? redirect()->route('password.reset')->with('status', trans($response))
                    : redirect()->back()->withErrors(['email' => trans($response)]);
    }

    public function showResetForm(Request $request, $token)
    {
        return Inertia::render('ForgotPassword/Reset', [
            'token' => $token,
            'email' => $request->email,
        ]);
    }

    public function reset(Request $request)
    {
        $request->validate([
            'otp' => 'required|string',
            'password' => 'required|confirmed|min:8',
        ]);

        $user = User::where('email', $request->email)->first();

        // Verify OTP
        if ($user && $user->otp === $request->otp) {
            // Reset the password
            $user->update([
                'password' => bcrypt($request->password),
                'otp' => null, // Clear the OTP after successful reset
            ]);

            return redirect()->route('home')->with('status', 'Password reset successfully.');
        }

        return redirect()->back()->withErrors(['otp' => 'Invalid OTP.']);
    }
}