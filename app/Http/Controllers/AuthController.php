<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function create(){
        return inertia(
            'Auth/Create'
        );
    }

    public function login(){
        return inertia(
            'Auth/login'
        );
    }

    public function store(Request $request){

        if (true){
            $request->session()->regenerate();

            return redirect('/home');
        }else{
            return back()->withErrors([
                'email' => 'The provided credentials do not match our records.',
            ])->onlyInput('email');
        }
    }

    public function destroy(Request $request){
        Auth::logout();

        return redirect('/login');
    }
}
