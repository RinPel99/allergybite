<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Mail;
use App\Mail\OtpMail;
use Str;
use Validator;

class RegistrationController extends Controller
{
    public $email;
    public function showRegistrationForm()
    {
        return Inertia('Auth/Registration');
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // Generate and save OTP
        $otp = Str::random(6);
        $password = Str::random(8);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role' => 'admin',
            'otp' => $otp,
        ]);
        session(['user_id' => $user->id]);
        $data = ['otp' => $otp];
        // Send OTP via email
        Mail::send('emails.otp', $data, function ($message) use($user) {
            $message
                ->to($user->email)
                ->subject('Your OTP for Password Reset');
        });
        
        return redirect()->route('registration.verify');
    }

    public function showVerificationForm()
    {
        return Inertia('Auth/Verification');
    }

    public function verify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::where('otp', $request->otp)->first();

        if ($user) {
            return redirect()->route('registration.password')->with('user_id', $user->id);
        }

        return redirect()->back()->withErrors(['otp' => 'Invalid OTP.']);
    }

    public function showPasswordForm(Request $request)
    {
        $user = User::find($request->session()->get('user_id'));

        return Inertia('Auth/Password', ['user' => $user]);
    }

    public function setPassword(Request $request)
{
    $validator = Validator::make($request->all(), [
        'password' => 'required|confirmed|min:8',
    ]);

    if ($validator->fails()) {
        return redirect()->back()->withErrors($validator)->withInput();
    }

    // Retrieve the user ID from the session
    $userId = $request->session()->get('user_id');
    // if ($userId === null) {
    //     // Log or debug the issue
    //     dd('user_id session variable is null');
    // }

    // Check if the user exists
    $user = User::find($userId);
    // Update the user's password and set OTP to null
    // $user->update([
    //     'password' => bcrypt($request->password),
    //     'otp' => null,
    // ]);

    return redirect()->route('registration.showAllergiesForm');
}

    public function showAllergiesForm()
    {
        return Inertia('Auth/Allergies');
    }

    public function setAllergies(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'allergies' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = User::find($request->session()->get('user_id'));

        // Convert comma-separated string to an array
        $allergies = explode(',', $request->allergies);

        // $user->update([
        //     'allergies' => $allergies,
        // ]);

        return redirect()->route('login');
    }
}