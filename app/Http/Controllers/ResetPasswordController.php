<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    public function showResetForm(Request $request, $token)
    {
        return view('auth.reset-password')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function reset(Request $request)
{
    $request->validate([
        'otp' => 'required|string',
        'password' => 'required|confirmed|min:8',
    ]);

    // Retrieve user by email
    $user = User::where('email', $request->email)->first();

    // Verify OTP
    if ($user && $user->otp === $request->otp) {
        // Reset the password
        $user->update([
            'password' => bcrypt($request->password),
            'otp' => null, // Clear the OTP after successful reset
        ]);

        // Log the user in after password reset if desired
        Auth::login($user);

        return redirect($this->redirectPath())->with('status', 'Password reset successfully.');
    }

    return back()->withErrors(['otp' => 'Invalid OTP.']);
    }
}
