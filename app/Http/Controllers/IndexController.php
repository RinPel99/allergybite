<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Classes;
use App\Models\Course;
use App\Models\Modules;
use App\Models\Prefrences;
use App\Models\PrjGroups;
use App\Models\WeeklyHours;
use App\Models\state;
use App\Models\TeachingRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function getLanding(){

        return inertia('index', [
        ]);
    }
    public function getDetail(){

        return inertia('Admin/detailed1', [
        ]);
    }
    public function getAdminHome(){

        return inertia('Admin/home', [
        ]);
    }
}


